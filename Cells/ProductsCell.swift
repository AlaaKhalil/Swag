//
//  ProductsCell.swift
//  Swag
//
//  Created by mahmoud farid on 4/24/18.
//  Copyright © 2018 mahmoud farid. All rights reserved.
//

import UIKit

class ProductsCell: UICollectionViewCell {
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    func updateView(product: Products){
        self.productImage.image = UIImage(named: "\(product.imageName)")
        self.productLabel.text = product.productName
        self.priceLabel.text = product.productPrice
    }
    
}
