//
//  CategoryCell.swift
//  Swag
//
//  Created by mahmoud farid on 4/24/18.
//  Copyright © 2018 mahmoud farid. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    
    func updateViews(category: Category)  {
        categoryImage.image = UIImage(named: category.imageName)
        categoryLabel.text = category.title
    }

}
