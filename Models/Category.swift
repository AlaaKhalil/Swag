//
//  Category.swift
//  Swag
//
//  Created by mahmoud farid on 4/24/18.
//  Copyright © 2018 mahmoud farid. All rights reserved.
//

import Foundation

struct Category{
    public private(set) var title: String
    public private(set) var imageName: String

    init(title: String, imageName: String) {
        self.imageName = imageName
        self.title = title
    }
    
}
