//
//  Products.swift
//  Swag
//
//  Created by mahmoud farid on 4/24/18.
//  Copyright © 2018 mahmoud farid. All rights reserved.
//

import Foundation

struct Products{
    public private(set) var imageName: String
    public private(set) var productName: String
    public private(set) var productPrice: String
    
    init(imageName: String, productName: String, productPrice: String) {
        self.imageName = imageName
        self.productName = productName
        self.productPrice = productPrice
    }

}
