//
//  Products.swift
//  Swag
//
//  Created by mahmoud farid on 4/24/18.
//  Copyright © 2018 mahmoud farid. All rights reserved.
//

import UIKit

class ProductsVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    private(set) public var products = [Products]()
    @IBOutlet weak var productCollection: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    func initProduct(category: Category){
        products = DataService.instance.getProducts(forCategory: category.title)
        navigationItem.title = category.title
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = productCollection.dequeueReusableCell(withReuseIdentifier: "productCell", for: indexPath) as? ProductsCell{
            let product = products[indexPath.row]
            cell.updateView(product: product)
            return cell
        }
        return ProductsCell()
    }

  

}
