//
//  DataService.swift
//  Swag
//
//  Created by mahmoud farid on 4/24/18.
//  Copyright © 2018 mahmoud farid. All rights reserved.
//

import Foundation

class DataService {
    static let instance = DataService()
    private let categories = [
        Category(title: "SHIRTS", imageName: "shirts.png"),
        Category(title: "HOODIES", imageName: "hoodies.png"),
        Category(title: "HATS", imageName: "hats.png"),
        Category(title: "DIGITAL", imageName: "digital.png"),
    ]
    private let shirts = [
    Products(imageName: "shirt01.png", productName: "Deveslopes Logo Graghic T-Shirt", productPrice: "25$"),
    Products(imageName: "shirt02.png", productName: "Deveslopes Logo Graghic T-Shirt", productPrice: "20$"),
    Products(imageName: "shirt03.png", productName: "Deveslopes Logo Graghic T-Shirt", productPrice: "30$"),
    Products(imageName: "shirt04.png", productName: "Deveslopes Logo Graghic T-Shirt", productPrice: "23$"),
    Products(imageName: "shirt05.png", productName: "Deveslopes Logo Graghic T-Shirt", productPrice: "23$")
    ]
    
    private let hats = [
    Products(imageName: "hat01", productName: "Deveslopes Logo Graghic hat", productPrice: "15&"),
    Products(imageName: "hat02", productName: "Deveslopes Logo Graghic hat", productPrice: "12&"),
    Products(imageName: "hat03", productName: "Deveslopes Logo Graghic hat", productPrice: "18&"),
    Products(imageName: "hat04", productName: "Deveslopes Logo Graghic hat", productPrice: "20&")
    ]
    
    private let hoodies = [
        Products(imageName: "hoodie01", productName: "Deveslopes Logo Graghic hoodie", productPrice: "30&"),
        Products(imageName: "hoodie02", productName: "Deveslopes Logo Graghic hoodie", productPrice: "40&"),
        Products(imageName: "hoodie03", productName: "Deveslopes Logo Graghic hoodie", productPrice: "36&"),
        Products(imageName: "hoodie04", productName: "Deveslopes Logo Graghic hoodie", productPrice: "29&")
    ]
    
    private let digitals = [Products]()
    
    
    func getCategory()-> [Category]{
        return categories
    }
    func getProducts(forCategory title: String) -> [Products]{
        switch title {
        case "SHIRTS":
            return shirts
        case "HATS":
            return hats
        case "HOODIES":
            return hoodies
        case "DIGITAL":
            return digitals
        default:
            return shirts
        }
        
    }
}
